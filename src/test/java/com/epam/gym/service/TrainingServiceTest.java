package com.epam.gym.service;

import com.epam.gym.dto.request.TrainingCreationDTO;
import com.epam.gym.entity.*;
import com.epam.gym.mapper.TrainingCreationMapper;
import com.epam.gym.repository.TraineeRepository;
import com.epam.gym.repository.TrainerRepository;
import com.epam.gym.repository.TrainingRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
class TrainingServiceTest {

    @Autowired
    private TrainingService trainingService;
    @MockBean
    private TrainingRepository trainingRepository;
    @MockBean
    private TraineeRepository traineeRepository;
    @MockBean
    private TrainerRepository trainerRepository;
    @MockBean
    private TrainingCreationMapper trainingCreationMapper;

    @Test
    @SuppressWarnings("null")
    void createTraining() {
        var trainingCreationDTO = new TrainingCreationDTO(
                "traineeUsername",
                "trainerUsername",
                "training",
                TrainingTypeEnum.STRETCHING,
                LocalDate.now(),
                5);

        var trainee = new Trainee(
                1L,
                "firstName",
                "lastName",
                trainingCreationDTO.traineeUsername(),
                "password",
                Role.TRAINEE,
                true,
                LocalDate.now(),
                "address",
                new HashSet<>(),
                List.of());

        var trainer = new Trainer(
                2L,
                "firstName",
                "lastName",
                trainingCreationDTO.trainerUsername(),
                "password",
                Role.TRAINEE,
                true,
                new TrainingType(TrainingTypeEnum.RESISTANCE),
                new HashSet<>(),
                List.of());

        var training = new Training(
                trainee,
                trainer,
                trainingCreationDTO.trainingName(),
                new TrainingType(trainingCreationDTO.type()),
                trainingCreationDTO.date(),
                trainingCreationDTO.duration());

        when(trainingCreationMapper.mapToTraining(any(TrainingCreationDTO.class))).thenReturn(training);
        when(trainingRepository.save(any(Training.class))).thenReturn(null);
        when(traineeRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(trainee));
        when(trainerRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(trainer));

        trainingService.create(trainingCreationDTO);

        verify(traineeRepository).findByUsernameIgnoreCase(trainee.getUsername());
        verify(trainerRepository).findByUsernameIgnoreCase(trainer.getUsername());
        verify(trainingRepository).save(training);
    }

}