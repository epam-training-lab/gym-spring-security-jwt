package com.epam.gym.service;

import com.epam.gym.exception.InvalidCredentialsException;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.request.PasswordChangeDTO;
import com.epam.gym.entity.User;
import com.epam.gym.repository.UserRepository;

import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

@Service
@Slf4j
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, @Lazy PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void checkCredentials(UserCredentialsDTO userCredentials) {
        if (invalidCredentials(userCredentials)) {
            log.warn("Invalid user credentials");
            throw new InvalidCredentialsException();
        }
    }

    public void setActive(User user, boolean isActive) {
        user.setActive(isActive);
        userRepository.save(user);
    }

    @Transactional
    public void changePassword(String username, PasswordChangeDTO passwordChangeDTO) {
        userRepository.changePassword(
                username,
                passwordEncoder.encode(passwordChangeDTO.newPassword()));

        log.info("Password changed. User: {}", username);
    }

    String generateUsername(String firstName, String lastName) {
        String username = firstName.toLowerCase() + "." + lastName.toLowerCase();
        int usernameCount = userRepository.countByUsernameStartingWith(username);

        if (usernameCount > 0) {
            username += usernameCount;
            log.debug("Username already exists. Serial number attached: {}", usernameCount);
        }

        return username;
    }

    String generatePassword() {
        final int PASSWORD_LENGTH = 10;
        final int ASCII_CODE_START = 33;
        final int ASCII_CODE_END = 126;

        var password = new StringBuilder(PASSWORD_LENGTH);
        var random = new Random();

        while (password.length() < PASSWORD_LENGTH) {
            int randomASCII = random.nextInt(ASCII_CODE_START, ASCII_CODE_END + 1);
            password.append((char) randomASCII);
        }

        return password.toString();
    }

    String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    public boolean invalidCredentials(UserCredentialsDTO userCredentials) {
        return !userRepository.existsByUsernameAndPassword(
                userCredentials.username(),
                passwordEncoder.encode(userCredentials.password()));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsernameIgnoreCase(username).orElseThrow(
            () -> new UsernameNotFoundException("User not found"));
    }

}
