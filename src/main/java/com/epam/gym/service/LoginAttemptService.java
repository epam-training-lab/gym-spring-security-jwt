package com.epam.gym.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import lombok.Getter;

@Service
public class LoginAttemptService {

    private final static int MAX_ATTEMPTS = 3;
    private final static long ATTEMPT_RESET_TIME = 5 * 60 * 1000;
    private final Map<String, AttemptInfo> attemptsCache = new ConcurrentHashMap<>();

    public void loginSucceeded(String key) {
        attemptsCache.remove(key);
    }

    public void loginFailed(String username) {
        AttemptInfo attemptInfo = attemptsCache.getOrDefault(username, new AttemptInfo());
        attemptInfo.incrementAttempts();
        attemptsCache.put(username, attemptInfo);
    }

    public boolean isBlocked(String username) {
        AttemptInfo attemptInfo = attemptsCache.get(username);

        if (attemptInfo == null) {
            return false;
        }

        long currentTime = System.currentTimeMillis();

        if (attemptInfo.getAttempts() >= MAX_ATTEMPTS && 
                (currentTime - attemptInfo.getLastAttemptTime() <= ATTEMPT_RESET_TIME)) {
            return true;
        } else if (currentTime - attemptInfo.getLastAttemptTime() > ATTEMPT_RESET_TIME) {
            loginSucceeded(username);
            return false;
        }

        return false;
    }

    @Getter
    private static class AttemptInfo {
        private int attempts;
        private long lastAttemptTime;

        public void incrementAttempts() {
            attempts++;
            lastAttemptTime = System.currentTimeMillis();
        }
    }

}
