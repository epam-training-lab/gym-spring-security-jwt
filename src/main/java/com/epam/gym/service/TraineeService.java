package com.epam.gym.service;

import com.epam.gym.config.PrometheusUserCountMetric;
import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.request.*;
import com.epam.gym.dto.response.TraineeProfileDTO;
import com.epam.gym.dto.response.TrainerListItemDTO;
import com.epam.gym.dto.response.TraineeTrainingDTO;
import com.epam.gym.entity.Role;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Trainer;
import com.epam.gym.entity.Training;
import com.epam.gym.entity.TrainingTypeEnum;
import com.epam.gym.exception.EntityNotFoundException;
import com.epam.gym.mapper.TraineeProfileMapper;
import com.epam.gym.mapper.TrainerListItemMapper;
import com.epam.gym.mapper.TrainingFindMapper;
import com.epam.gym.mapper.UsernameTrainerMapper;
import com.epam.gym.repository.TraineeRepository;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class TraineeService {

    private final TraineeRepository traineeRepository;
    private final UserService userService;
    private final TrainingService trainingService;
    private final UsernameTrainerMapper usernameTrainerMapper;
    private final PrometheusUserCountMetric prometheusUserCountMetric;

    @Autowired
    public TraineeService(TraineeRepository traineeRepository, UserService userService,
            TrainingService trainingService, UsernameTrainerMapper usernameTrainerMapper,
            PrometheusUserCountMetric prometheusUserCountMetric) {
        this.traineeRepository = traineeRepository;
        this.userService = userService;
        this.trainingService = trainingService;
        this.usernameTrainerMapper = usernameTrainerMapper;
        this.prometheusUserCountMetric = prometheusUserCountMetric;
    }

    public UserCredentialsDTO registerTrainee(TraineeRegistrationDTO traineeDTO) {
        String password = userService.generatePassword();

        var trainee = new Trainee(
                traineeDTO.firstName(),
                traineeDTO.lastName(),
                userService.generateUsername(
                    traineeDTO.firstName(),
                    traineeDTO.lastName()),
                userService.encodePassword(password),
                Role.TRAINEE,
                true,
                traineeDTO.dateOfBirth(),
                traineeDTO.address());

        trainee = traineeRepository.save(trainee);
        prometheusUserCountMetric.updateCount();

        log.info("New trainee created: UserID={}", trainee.getUserId());
        return new UserCredentialsDTO(trainee.getUsername(), password);
    }

    @Transactional
    public TraineeProfileDTO update(String username,
            TraineeUpdateDTO traineeUpdateInfo) {
        Trainee trainee = select(username);

        trainee = new Trainee(
                trainee.getUserId(),
                traineeUpdateInfo.firstName(),
                traineeUpdateInfo.lastName(),
                username,
                trainee.getPassword(),
                Role.TRAINEE,
                traineeUpdateInfo.isActive(),
                traineeUpdateInfo.dateOfBirth(),
                traineeUpdateInfo.address(),
                trainee.getTrainers(),
                trainee.getTrainings());

        trainee = traineeRepository.save(trainee);

        log.info("Trainee updated: UserID={}", trainee.getUserId());
        return TraineeProfileMapper.traineeToTraineeProfileDTO(trainee);
    }

    @Transactional
    public List<TrainerListItemDTO> updateTrainers(String username,
            TraineeTrainersUpdateDTO traineeTrainersDTO) {
        Trainee trainee = select(username);

        List<Trainer> trainers = traineeTrainersDTO.trainers().stream()
                .map(usernameTrainerMapper)
                .toList();

        trainee.getTrainers().forEach(trainer -> trainer.getTrainees().remove(trainee));
        trainee.getTrainers().clear();
        trainee.getTrainers().addAll(trainers);
        trainers.forEach(trainer -> trainer.getTrainees().add(trainee));
        traineeRepository.save(trainee);

        log.info("Trainee updated: UserID={}", trainee.getUserId());
        return trainers.stream()
                .map(TrainerListItemMapper.map)
                .toList();
    }

    public void setActive(String username, UserSetActiveDTO setActiveDTO) {
        Trainee trainee = select(username);
        userService.setActive(trainee, setActiveDTO.isActive());
        log.info("Trainee 'active' set to {}: UserID={}",
        setActiveDTO.isActive(), trainee.getUserId());
    }

    public Trainee changePassword(String username, PasswordChangeDTO passwordChangeDTO) {
        userService.changePassword(username, passwordChangeDTO);
        Trainee trainee = select(username);
        log.info("Trainee password changed: UserID={}", trainee.getUserId());
        return trainee;
    }

    public Trainee select(String username) {
        return traineeRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new EntityNotFoundException("Trainee not found"));
    }

    @Transactional
    public TraineeProfileDTO getTraineeProfile(String username) {
        return TraineeProfileMapper.traineeToTraineeProfileDTO(select(username));
    }

    @Transactional
    public void delete(UserCredentialsDTO userCredentials, String username) {
        Trainee trainee = select(username);
        trainingService.deleteAll(trainee.getTrainings());
        trainee.getTrainers().forEach(trainer -> trainer.getTrainees().remove(trainee));
        trainee.getTrainers().clear();
        traineeRepository.delete(trainee);
        log.info("Trainee deleted: UserID=" + trainee.getUserId());
        prometheusUserCountMetric.updateCount();
    }

    @Transactional
    @PreAuthorize("#username == authentication.principal.username or hasAuthority('ROLE_TRAINER')")
    public List<TraineeTrainingDTO> getTrainings(String username, LocalDate fromDate,
            LocalDate toDate, String trainerName, TrainingTypeEnum type) {
        List<Training> traineeTrainings = select(username).getTrainings();

        if (type != null)
            traineeTrainings.retainAll(getTrainingsByType(traineeTrainings, type));
        if (fromDate != null)
            traineeTrainings.retainAll(getTrainingsFromDate(traineeTrainings, fromDate));
        if (toDate != null)
            traineeTrainings.retainAll(getTrainingsToDate(traineeTrainings, toDate));
        if (trainerName != null)
            traineeTrainings.retainAll(getTrainingsByTrainerName(traineeTrainings, trainerName));

        return traineeTrainings.stream().map(TrainingFindMapper.mapTrainee).toList();
    }

    private List<Training> getTrainingsByType(List<Training> traineeTrainings, TrainingTypeEnum type) {
        return traineeTrainings.stream()
                .filter(training -> training.getType().getName().equals(type))
                .toList();
    }

    private List<Training> getTrainingsFromDate(List<Training> traineeTrainings, LocalDate fromDate) {
        return traineeTrainings.stream()
                .filter(training -> !training.getDate().isBefore(fromDate))
                .toList();
    }

    private List<Training> getTrainingsToDate(List<Training> traineeTrainings, LocalDate toDate) {
        return traineeTrainings.stream()
                .filter(training -> !training.getDate().isAfter(toDate))
                .toList();
    }

    private List<Training> getTrainingsByTrainerName(List<Training> traineeTrainings, String trainerName) {
        return traineeTrainings.stream()
                .filter(training -> training
                        .getTrainer()
                        .getFirstName()
                        .equalsIgnoreCase(trainerName))
                .toList();
    }

}
