package com.epam.gym.service;

import com.epam.gym.config.PrometheusUserCountMetric;
import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.request.*;
import com.epam.gym.dto.response.TrainerListItemDTO;
import com.epam.gym.dto.response.TrainerProfileDTO;
import com.epam.gym.dto.response.TrainerTrainingDTO;
import com.epam.gym.entity.Training;
import com.epam.gym.entity.TrainingTypeEnum;
import com.epam.gym.exception.EntityNotFoundException;
import com.epam.gym.mapper.TrainerListItemMapper;
import com.epam.gym.mapper.TrainerProfileMapper;
import com.epam.gym.mapper.TrainingFindMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gym.entity.Role;
import com.epam.gym.entity.Trainer;
import com.epam.gym.repository.TrainerRepository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.time.LocalDate;

@Service
@Slf4j
public class TrainerService {

    private final TrainerRepository trainerRepository;
    private final UserService userService;
    private final TrainingTypeService trainingTypeService;
    private final PrometheusUserCountMetric prometheusUserCountMetric;

    @Autowired
    public TrainerService(TrainerRepository trainerDAO, UserService userService,
                TrainingTypeService trainingTypeService, 
                PrometheusUserCountMetric prometheusUserCountMetric) {
        this.trainerRepository = trainerDAO;
        this.userService = userService;
        this.trainingTypeService = trainingTypeService;
        this.prometheusUserCountMetric = prometheusUserCountMetric;
    }

    @Transactional
    public UserCredentialsDTO registerTrainer(TrainerRegistrationDTO trainerDTO) {
        String password = userService.generatePassword();

        var trainer = new Trainer(
                trainerDTO.firstName(),
                trainerDTO.lastName(),
                userService.generateUsername(
                        trainerDTO.firstName(),
                        trainerDTO.lastName()),
                userService.encodePassword(password),
                Role.TRAINER,
                true,
                trainingTypeService.getTrainingType(trainerDTO.specialization()));

        trainer = trainerRepository.save(trainer);
        prometheusUserCountMetric.updateCount();

        log.info("New trainer created: UserID=" + trainer.getUserId());
        return new UserCredentialsDTO(trainer.getUsername(), password);
    }

    @Transactional
    public TrainerProfileDTO update(String username,
            TrainerUpdateDTO trainerUpdateInfo) {
        Trainer trainer = select(username);

        trainer = new Trainer(
                trainer.getUserId(),
                trainerUpdateInfo.firstName(),
                trainerUpdateInfo.lastName(),
                username,
                trainer.getPassword(),
                Role.TRAINER,
                trainerUpdateInfo.isActive(),
                trainingTypeService.getTrainingType(
                        trainerUpdateInfo.specialization()),
                trainer.getTrainees(),
                trainer.getTrainings());

        trainer = trainerRepository.save(trainer);

        log.info("Trainer updated: UserID={}", trainer.getUserId());
        return TrainerProfileMapper.trainerToTrainerProfileDTO(trainer);
    }

    public void setActive(String username, UserSetActiveDTO setActiveDTO) {
        Trainer trainer = select(username);
        userService.setActive(trainer, setActiveDTO.isActive());
        log.info("Trainer 'active' set to {}: UserID={}", setActiveDTO.isActive(), trainer.getUserId());
    }

    public Trainer changePassword(String username, PasswordChangeDTO passwordChangeDTO) {
        userService.changePassword(username, passwordChangeDTO);
        Trainer trainer = select(username);
        log.info("Trainer password changed: UserID={}", trainer.getUserId());
        return trainer;
    }

    public Trainer select(String username) {
        return trainerRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new EntityNotFoundException("Trainer not found"));
    }

    @Transactional
    public TrainerProfileDTO getTrainerProfile(String username) {
        return TrainerProfileMapper.trainerToTrainerProfileDTO(select(username));
    }

    @Transactional
    public List<TrainerTrainingDTO> getTrainings(String username, LocalDate fromDate, LocalDate toDate,
            String traineeName, TrainingTypeEnum type) {
        List<Training> trainerTrainings = select(username).getTrainings();

        if (type != null)
            trainerTrainings.retainAll(getTrainingsByType(trainerTrainings, type));
        if (fromDate != null)
            trainerTrainings.retainAll(getTrainingsFromDate(trainerTrainings, fromDate));
        if (toDate != null)
            trainerTrainings.retainAll(getTrainingsToDate(trainerTrainings, toDate));
        if (traineeName != null)
            trainerTrainings.retainAll(getTrainingsByTraineeName(trainerTrainings, traineeName));

        return trainerTrainings.stream().map(TrainingFindMapper.mapTrainer).toList();
    }

    private List<Training> getTrainingsByType(List<Training> traineeTrainings, TrainingTypeEnum type) {
        return traineeTrainings.stream()
                .filter(training -> training.getType().getName().equals(type))
                .toList();
    }

    private List<Training> getTrainingsFromDate(List<Training> traineeTrainings, LocalDate fromDate) {
        return traineeTrainings.stream()
                .filter(training -> !training.getDate().isBefore(fromDate))
                .toList();
    }

    private List<Training> getTrainingsToDate(List<Training> traineeTrainings, LocalDate toDate) {
        return traineeTrainings.stream()
                .filter(training -> !training.getDate().isAfter(toDate))
                .toList();
    }

    public List<Training> getTrainingsByTraineeName(List<Training> trainerTrainings, String traineeName) {
        return trainerTrainings.stream()
                .filter(training -> training
                        .getTrainee()
                        .getFirstName()
                        .equalsIgnoreCase(traineeName))
                .toList();
    }

    public List<TrainerListItemDTO> getNotAssignActiveTrainers() {
        return trainerRepository.findNotAssignedActive().stream()
                .map(TrainerListItemMapper.map)
                .toList();
    }

}
