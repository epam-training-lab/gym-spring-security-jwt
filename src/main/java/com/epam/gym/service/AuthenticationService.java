package com.epam.gym.service;

import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.response.AuthenticationResponseDTO;
import com.epam.gym.exception.UserBlockedException;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthenticationService {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final LoginAttemptService loginAttemptService;
    private final JwtService jwtService;

    @Autowired
    public AuthenticationService(UserService userService, AuthenticationManager authenticationManager,
            LoginAttemptService loginAttemptService, JwtService jwtService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.loginAttemptService = loginAttemptService;
        this.jwtService = jwtService;
    }

    public AuthenticationResponseDTO authenticate(UserCredentialsDTO userCredentials) {
        if (loginAttemptService.isBlocked(userCredentials.username())) {
            log.warn("Blocked user is trying to login: username={}", userCredentials.username());
            throw new UserBlockedException();
        }

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                userCredentials.username(), userCredentials.password()));

        log.info("User authenticated: {}", userCredentials.username());

        return new AuthenticationResponseDTO(
                jwtService.generateToken(
                        userService.loadUserByUsername(
                                userCredentials.username())));
    }
}
