package com.epam.gym.mapper;

import com.epam.gym.dto.response.TraineeProfileDTO;
import com.epam.gym.entity.Trainee;

import java.util.List;

public class TraineeProfileMapper {

    public static TraineeProfileDTO traineeToTraineeProfileDTO(Trainee trainee) {
        return new TraineeProfileDTO(
                trainee.getFirstName(),
                trainee.getLastName(),
                trainee.getDateOfBirth(),
                trainee.getAddress(),
                trainee.isActive(),
                trainee.getTrainers() == null ? List.of() :
                        trainee.getTrainers().stream().map(TrainerListItemMapper.map).toList());
    }

}
