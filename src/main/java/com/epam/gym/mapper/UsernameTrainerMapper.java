package com.epam.gym.mapper;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.epam.gym.entity.Trainer;
import com.epam.gym.service.TrainerService;

@Component
public class UsernameTrainerMapper implements Function<String, Trainer> {

    private final TrainerService trainerService;

    public UsernameTrainerMapper(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    @Override
    public Trainer apply(String username) {
        return trainerService.select(username);
    }
    
}
