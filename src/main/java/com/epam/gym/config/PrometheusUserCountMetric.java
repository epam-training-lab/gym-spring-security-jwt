package com.epam.gym.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.epam.gym.repository.UserRepository;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Gauge;
import jakarta.annotation.PostConstruct;

@Component
public class PrometheusUserCountMetric {

    private final UserRepository userRepository;
    private final Gauge userCount;

    @Autowired
    public PrometheusUserCountMetric(UserRepository userRepository, CollectorRegistry collectorRegistry) {
        this.userRepository = userRepository;
        userCount = Gauge
                .build("custom_user_count", "Current number of registered users")
                .register(collectorRegistry);
    }

    @PostConstruct
    public void initializeGauge() {
        updateCount();
    }

    public void updateCount() {
        userCount.set(userRepository.count());
    }

}
