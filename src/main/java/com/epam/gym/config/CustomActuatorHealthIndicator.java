package com.epam.gym.config;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import com.epam.gym.repository.UserRepository;

@Component("database-registered-users")
public class CustomActuatorHealthIndicator implements HealthIndicator {

    private final UserRepository userRepository;
    
    @Autowired
    public CustomActuatorHealthIndicator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Health health() {
        Health.Builder builder = new Health.Builder();
        try {
            boolean serviceUp = check(); 

            if (serviceUp) {
                builder.up();
            } else {
                builder.down();
            }
            
            builder.withDetail("DB running", check());
            builder.withDetail("Registered user count", userCount());

            builder.withDetail("Users", getUsers());
        } catch (Exception e) {
            builder.down(e);
        }

        return builder.build();
    }

    private boolean check() {
        try {
            userRepository.count();
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    private long userCount() {
        return userRepository.count();
    }

    private List<String> getUsers() {
        List<String> users = new LinkedList<>();
        userRepository.findAll().forEach(user -> users.add(user.getUsername()));
        return users;
    }

}
