package com.epam.gym.dto.request;

import jakarta.validation.constraints.NotNull;

public record UserSetActiveDTO(
        @NotNull Boolean isActive) {

}
