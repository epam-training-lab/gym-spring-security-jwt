package com.epam.gym.dto.request;

import com.epam.gym.entity.TrainingTypeEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record TrainerRegistrationDTO(
        @NotBlank String firstName,
        @NotBlank String lastName,
        @NotNull TrainingTypeEnum specialization) {

}
