package com.epam.gym.dto.request;

import jakarta.validation.constraints.NotBlank;

public record PasswordChangeDTO(
        @NotBlank String newPassword) {

}
