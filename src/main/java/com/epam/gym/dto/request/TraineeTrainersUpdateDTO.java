package com.epam.gym.dto.request;

import java.util.List;

import jakarta.validation.constraints.NotNull;

public record TraineeTrainersUpdateDTO(
        @NotNull List<String> trainers) {

}
