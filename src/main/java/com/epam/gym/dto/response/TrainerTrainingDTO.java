package com.epam.gym.dto.response;

import java.time.LocalDate;

import com.epam.gym.entity.TrainingTypeEnum;

public record TrainerTrainingDTO(
        String name,
        LocalDate date,
        TrainingTypeEnum type,
        int duration,
        String traineeName) {
    
}
