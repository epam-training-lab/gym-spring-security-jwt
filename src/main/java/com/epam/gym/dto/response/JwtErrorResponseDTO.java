package com.epam.gym.dto.response;

public record JwtErrorResponseDTO(
        String error,
        String message) {

}
