package com.epam.gym.dto.response;

import com.epam.gym.entity.TrainingTypeEnum;

public record TrainingTypeDTO(
        Long id,
        TrainingTypeEnum type) {

}
