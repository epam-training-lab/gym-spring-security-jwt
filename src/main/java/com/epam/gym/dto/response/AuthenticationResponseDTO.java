package com.epam.gym.dto.response;

public record AuthenticationResponseDTO(
        String token) {

}
