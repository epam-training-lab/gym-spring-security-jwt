package com.epam.gym.repository;

import com.epam.gym.entity.TrainingType;

import com.epam.gym.entity.TrainingTypeEnum;
import jakarta.annotation.Nonnull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TrainingTypeRepository extends CrudRepository<TrainingType, Long> {

    Optional<TrainingType> findByName(TrainingTypeEnum name);
    @Nonnull List<TrainingType> findAll();

}
