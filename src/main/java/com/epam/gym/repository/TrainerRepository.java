package com.epam.gym.repository;

import com.epam.gym.entity.Trainer;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TrainerRepository extends CrudRepository<Trainer, Long> {

    Optional<Trainer> findByUsernameIgnoreCase(String username);

    @Query(value = """
                SELECT * FROM trainer t
                JOIN app_user au ON t.id = au.id
                LEFT JOIN trainee2trainer tt ON t.id = tt.trainer_id
                WHERE au.is_active = true AND tt.trainer_id IS NULL
            """, nativeQuery = true)
    List<Trainer> findNotAssignedActive();

}
