package com.epam.gym.controller;

import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.request.*;
import com.epam.gym.dto.response.TraineeProfileDTO;
import com.epam.gym.dto.response.TrainerListItemDTO;
import com.epam.gym.dto.response.TraineeTrainingDTO;
import com.epam.gym.entity.TrainingTypeEnum;
import com.epam.gym.service.TraineeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/trainees")
@Tag(name = "Trainee", description = "API for trainees")
public class TraineeController {

    private final TraineeService traineeService;

    @Autowired
    public TraineeController(TraineeService traineeService) {
        this.traineeService = traineeService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Register a new trainee")
    public UserCredentialsDTO registerTrainee(
            @Valid @RequestBody TraineeRegistrationDTO traineeRegistrationDTO) {
        return traineeService.registerTrainee(traineeRegistrationDTO);
    }

    @GetMapping("/{username}")
    @Operation(summary = "Get trainee profile by username")
    public TraineeProfileDTO getTraineeProfile(@PathVariable String username) {
        return traineeService.getTraineeProfile(username);
    }

    @PutMapping("/{username}")
    @Operation(summary = "Update trainee profile")
    public TraineeProfileDTO updateTraineeProfile(@PathVariable String username,
            @Valid @RequestBody TraineeUpdateDTO traineeUpdateDTO) {
        return traineeService.update(username, traineeUpdateDTO);
    }

    @DeleteMapping("/{username}")
    @Operation(summary = "Delete a trainee by username")
    public void deleteTrainee(@PathVariable String username,
            @Valid @RequestBody UserCredentialsDTO userCredentials) {
        traineeService.delete(userCredentials, username);
    }

    @PutMapping("/{username}/trainers")
    @Operation(summary = "Update trainers for a trainee")
    public List<TrainerListItemDTO> updateTrainers(@PathVariable String username,
            @Valid @RequestBody TraineeTrainersUpdateDTO traineeTrainersUpdateDTO) {
        return traineeService.updateTrainers(username, traineeTrainersUpdateDTO);
    }

    @GetMapping("/{username}/trainings")
    @Operation(summary = "Get trainings for a trainee")
    public List<TraineeTrainingDTO> getTrainings(
            @PathVariable String username,
            @RequestParam(required = false) LocalDate periodFrom,
            @RequestParam(required = false) LocalDate periodTo,
            @RequestParam(required = false) String trainerName,
            @RequestParam(required = false) TrainingTypeEnum type) {
        return traineeService.getTrainings(username, periodFrom, periodTo, trainerName, type);
    }

    @PatchMapping("{username}/active")
    @Operation(summary = "Set the 'active' status for a trainee")
    public void setTraineeActive(@PathVariable String username,
            @Valid @RequestBody UserSetActiveDTO setActiveDTO) {
        traineeService.setActive(username, setActiveDTO);
    }

}
