package com.epam.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gym.dto.request.PasswordChangeDTO;
import com.epam.gym.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/users")
@Tag(name = "User", description = "API for users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    
    @PutMapping("/{username}/password-change")
    @Operation(summary = "Change password for a user")
    public void changePassword(@PathVariable String username,
                @Validated @RequestBody PasswordChangeDTO securePasswordChangeDTO) {
        userService.changePassword(username, securePasswordChangeDTO);
    }
    
}
