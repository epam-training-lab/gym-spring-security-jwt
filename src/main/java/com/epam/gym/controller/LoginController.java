package com.epam.gym.controller;

import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.response.AuthenticationResponseDTO;
import com.epam.gym.service.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/login")
@Tag(name = "Login", description = "API for login")
public class LoginController {

    private final AuthenticationService authenticationService;

    @Autowired
    public LoginController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping
    @Operation(summary = "Login a registered user")
    public AuthenticationResponseDTO login(@RequestBody UserCredentialsDTO userCredentials) {
        return authenticationService.authenticate(userCredentials);
    }

}
