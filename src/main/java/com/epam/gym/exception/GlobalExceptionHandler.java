package com.epam.gym.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({
            InvalidCredentialsException.class,
            BadCredentialsException.class,
            DisabledException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Map<String, Object> handleInvalidCredentialsException(RuntimeException e) {
        String message;

        if (e instanceof DisabledException) {
            message = "User disabled.";
            log.warn(message);
        } else {
            message = "Invalid user credentials.";
        }

        var error = new LinkedHashMap<String, Object>();
        error.put("timestamp", LocalDateTime.now());
        error.put("status", HttpStatus.UNAUTHORIZED.value());
        error.put("message", message);
        return error;
    }

    @ExceptionHandler(UserBlockedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Map<String, Object> handleUserBlockedException(UserBlockedException e) {
        var error = new LinkedHashMap<String, Object>();
        error.put("timestamp", LocalDateTime.now());
        error.put("status", HttpStatus.UNAUTHORIZED.value());
        error.put("error", "User temporarily blocked");
        error.put("message", "Too many failed login attemps.");
        return error;
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, Object> handleUserNotFoundException(EntityNotFoundException e) {
        var error = new LinkedHashMap<String, Object>();
        error.put("timestamp", LocalDateTime.now());
        error.put("status", HttpStatus.NOT_FOUND.value());
        error.put("message", e.getMessage());
        return error;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleValidationExceptions(MethodArgumentNotValidException e) {
        var errors = new LinkedHashMap<String, Object>();
        var fieldErrors = new HashMap<String, String>();

        errors.put("timestamp", LocalDateTime.now());
        errors.put("status", HttpStatus.BAD_REQUEST.value());
        errors.put("message", "Fields have invalid values.");

        e.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            fieldErrors.put(fieldName, errorMessage);
        });

        errors.put("fields", fieldErrors);
        return errors;
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleHttpMessageNotReadableExceptions(HttpMessageNotReadableException ex) {
        Throwable rootCause = ex.getRootCause();
        var error = new LinkedHashMap<String, Object>();
        error.put("timestamp", LocalDateTime.now());
        error.put("status", HttpStatus.BAD_REQUEST.value());
        error.put("error", "Bad Request");

        if (rootCause instanceof DateTimeParseException) {
            error.put("message", "Invalid date format. Please use the format YYYY-MM-DD.");
        } else if (rootCause instanceof InvalidFormatException invalidFormatException) {
            if (invalidFormatException.getTargetType().isEnum()) {
                error.put("message", String.format("Provided '%s' value is invalid.",
                        invalidFormatException.getValue().toString()));
            }
        } else if (rootCause != null) {
            error.put("message", rootCause.getMessage());
        }

        return error;
    }

}
