package com.epam.gym.exception;

public class UserBlockedException extends RuntimeException {
    
    public UserBlockedException() {
        super("User blocked");
    }

}
