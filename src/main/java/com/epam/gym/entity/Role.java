package com.epam.gym.entity;

public enum Role {
    TRAINEE,
    TRAINER
}
