package com.epam.gym.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "trainee")
@Getter
@NoArgsConstructor
public class Trainee extends User {

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "address")
    private String address;

    @ManyToMany(mappedBy = "trainees")
    private Set<Trainer> trainers;

    @OneToMany(mappedBy = "trainee")
    private List<Training> trainings;

    public Trainee(String firstName, String lastName, String username, String password,
            Role role, boolean isActive, LocalDate dateOfBirth, String address) {
        super(firstName, lastName, username, password, role, isActive);
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }

    public Trainee(Long userId, String firstName, String lastName, String username,
            String password, Role role, boolean isActive, LocalDate dateOfBirth, String address,
            Set<Trainer> trainers, List<Training> trainings) {
        super(userId, firstName, lastName, username, password, role, isActive);
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.trainers = trainers;
        this.trainings = trainings;
    }

}
